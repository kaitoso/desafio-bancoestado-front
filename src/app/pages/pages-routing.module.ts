import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { TableComponent } from './table/table.component';

const routes: Routes = [
  {path: '' , component: HomeComponent,
  children: [
    {path: 'characters', component: TableComponent},
    {path: '**', redirectTo: 'characters'}]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
