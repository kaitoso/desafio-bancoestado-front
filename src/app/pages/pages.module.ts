import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { HomeComponent } from './home.component';
import { TableComponent } from './table/table.component';
import { HeaderComponent } from './header/header.component';
import { MaterialModule } from '../material/material.module';


@NgModule({
  declarations: [HomeComponent, TableComponent, HeaderComponent],
  imports: [
    CommonModule,
    PagesRoutingModule,
    MaterialModule
  ]
})
export class PagesModule { }
