import { Component, Input, OnInit, ViewChild } from '@angular/core';

import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { CharactersService } from 'src/app/services/characters.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  paginas = [5, 10];
  displayedColumns: string[] = ['imagen','nombre', 'status','especie','genero','origen'];
  dataSource: MatTableDataSource<any>;
  characters:any;


  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  
  
  constructor(private cs: CharactersService) { 
    this.dataSource = new MatTableDataSource();
    this.cs.getCharacters().subscribe(res =>{
       this.characters = res;
       this.dataSource.data = this.characters.characters;
    });

  }

  ngOnInit(): void {
   
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getCharacters(){
    this.cs.getCharacters().subscribe(res =>{
      this.characters = res;
    });
  }

}
